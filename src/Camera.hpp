#pragma once

//#include "rtweekend.hpp"
#include "Vec3.hpp"
#include "Ray.hpp"

class Camera
{
public:
	Camera(
		Vec3 lookfrom, Vec3 lookat, Vec3 vup,
		double vfov, // vertical filed of view in degrees
		double aspect_ratio, double aperture, double focus_dist
		)
	{
		origin = lookfrom;
		lens_radius = aperture / 2;

		auto theta = degToRad(vfov);
		auto half_height = tan(theta * 0.5);
		auto half_width = aspect_ratio * half_height;
		w = unit(lookfrom - lookat);
		u = unit(cross(vup, w));
		v = unit(cross(w, u));

		lower_left = origin 
					- half_width * focus_dist * u
					- half_height * focus_dist * v
					- focus_dist * w;
		horizontal = 2 * half_width * focus_dist * u;
		vertical = 2 * half_height * focus_dist * v;
	}

	Ray getRay(double s, double t)
	{
		Vec3 rd = lens_radius * randomInUnitDisk();
		Vec3 offset = u * rd.x() + v * rd.y();

		Ray r(	origin + offset, 
				lower_left + s * horizontal + t * vertical - origin - offset);
		return r;
	}

private:
	Vec3 origin, lower_left, horizontal, vertical;
	Vec3 u, v, w;
	double lens_radius;
};