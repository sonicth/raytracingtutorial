#pragma once

#include "Ray.hpp"
#include <memory>

class Material;
using Material_p = std::shared_ptr<Material>;

struct HitRecord
{
	
	Vec3 p;
	Vec3 normal;
	Material_p material_ptr;
	double t;
	bool front_face;

	void setFaceNormal(Ray const &r, Vec3 const &outward_normal)
	{
		front_face = dot(r.direction(), outward_normal) < 0;
		normal = front_face ? outward_normal : -outward_normal;
	}
};

class Hittable
{
public:
	virtual bool hit(Ray const &r, double t_min, double t_max, HitRecord &rec) const = 0;

};