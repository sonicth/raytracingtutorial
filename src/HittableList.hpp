#pragma once

#include "Hittable.hpp"
#include <memory>
#include <vector>

using Hittable_p = std::shared_ptr<Hittable>;

class HittableList:
	public Hittable
{
public:
	HittableList() = default;
	HittableList(Hittable_p object) { add(object); }

	void clear() { objects.clear(); }
	void add(Hittable_p object) { objects.push_back(object); }
	virtual bool hit(Ray const &r, double t_min, double t_max, HitRecord &rec) const override;
	
public:
	std::vector<Hittable_p> objects;

};