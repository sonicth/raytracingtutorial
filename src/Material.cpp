#include "Material.hpp"
#include "Vec3.hpp"
#include "Hittable.hpp"

bool Lambertian::scatter(Ray const &r_in, HitRecord const &rec, Colour &attenuation, Ray &scattered) const
{
	Vec3 scatter_direction = rec.normal + randomUnitVector();
	scattered = Ray(rec.p, scatter_direction);
	attenuation = albedo;
	return true;
}

bool Metal::scatter(Ray const &r_in, HitRecord const &rec, Colour &attenuation, Ray &scattered) const
{
	Vec3 reflected = reflect(rec.normal, unit(r_in.direction()));
	scattered = Ray(rec.p, reflected + randomInUnitSphere() * fuzz);
	attenuation = albedo;
	return (dot(scattered.direction() , rec.normal) > 0);
}

double schlick(double cosine, double refractive_idx)
{
	auto r0 = (1 - refractive_idx) / (1 + refractive_idx);
	auto r0sq = r0 * r0;
	return r0sq + (1 - r0sq) * pow((1 - cosine), 5);
}

bool Glass::scatter(Ray const &r_in, HitRecord const &rec, Colour &attenuation, Ray &scattered) const
{
	double etai_over_etat = rec.front_face ? 1.0 / refractive_idx : refractive_idx;
	Vec3 unit_direction = unit(r_in.direction());

	double cos_theta = fmin(dot(-unit_direction, rec.normal), 1.0);
	double sin_theta = sqrt(1.0 - cos_theta * cos_theta);

	if (etai_over_etat * sin_theta > 1)
	{
		Vec3 reflected = reflect(rec.normal, unit_direction);
		scattered = Ray(rec.p, reflected);
	}
	else if (randomDouble() < schlick(cos_theta, etai_over_etat))
	{
		Vec3 reflected = reflect(rec.normal, unit_direction);
		scattered = Ray(rec.p, reflected);
	}
	else
	{
		Vec3 refracted = refract_book(rec.normal, unit_direction, etai_over_etat);
		scattered = Ray(rec.p, refracted);
	}

	//attenuation = albedo;
	attenuation = Vec3(1, 1, 1);

	return true;
}
