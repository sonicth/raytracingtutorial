#pragma once
#include "Vec3.hpp"


class Ray;
struct HitRecord;


class Material
{
public:
	virtual bool scatter(Ray const &r_in, HitRecord const &rec, Colour &attenuation, Ray &scattered) const = 0;
};

class Lambertian
	: public Material
{
public:
	Lambertian(Colour const &c)
		: albedo{ c }
	{}

public:
	Colour albedo;

	virtual bool scatter(Ray const &r_in, HitRecord const &rec, Colour &attenuation, Ray &scattered) const override;
};

class Metal
	: public Material
{
public:
	Metal(Colour colour_, double fuzz_)
		: albedo{ colour_ }, fuzz{ fuzz_ > 1 ? 1 : fuzz_ }
	{}


private:
	Colour albedo;
	double fuzz;

	virtual bool scatter(Ray const &r_in, HitRecord const &rec, Colour &attenuation, Ray &scattered) const override;
};

class Glass
	: public Material
{
public:
	Glass(Colour colour_, double refractive_idx_, double opacity_)
		: albedo{ colour_ }
		, refractive_idx{ refractive_idx_ }
		, opacity{ opacity_ > 1 ? 1 : opacity_ }
	{}

private:
	Colour albedo;
	double refractive_idx, opacity;

	virtual bool scatter(Ray const &r_in, HitRecord const &rec, Colour &attenuation, Ray &scattered) const override;
};
