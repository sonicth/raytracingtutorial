#pragma once
#include "Vec3.hpp"

class Ray
{
public:
	Ray() {}
	Ray(Vec3 const &origin_, Vec3 const &direction_)
		: _origin{ origin_ }
		, _direction{direction_}
	{}
	Vec3 origin() const { return _origin; }
	Vec3 direction() const { return _direction; }

	Vec3 at(double t) const { return origin() + t * direction(); }
	
public:
	Vec3 _origin, _direction;
};
