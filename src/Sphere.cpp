#include "Sphere.hpp"



bool Sphere::hit(Ray const &r, double t_min, double t_max, HitRecord &rec) const
{
	Vec3 oc = r.origin() - centre; // (a - c)

	auto a = dot(r.direction(), r.direction());
	auto half_b = dot(r.direction(), oc);
	auto c = dot(oc, oc) - radius * radius;

	auto discriminant = half_b * half_b - a * c;
	if (discriminant > 0)
	{
		auto hitHelper = [this, &r, t_min, t_max, &rec] (double t)
		{	
			if (t_min < t && t < t_max)
			{
				rec.t = t;
				rec.p = r.at(rec.t);
				auto outward_normal = (rec.p - centre) / radius;
				rec.setFaceNormal(r, outward_normal);
				rec.material_ptr = material_ptr;
				return true;
			}
			else
				return false;
		};
				
		auto root = sqrt(discriminant);
		return	hitHelper((-half_b - root) / a) || 
				hitHelper((-half_b + root) / a);
;
	}
	else
		return false;
}
