#pragma once

#include "Hittable.hpp"
#include "Vec3.hpp"

class Sphere
	: public Hittable
{
public:
	Sphere() = default;
	Sphere(Vec3 centre_, double radius_, Material_p material_ptr_)
		: centre{ centre_ }, radius{ radius_ }
		, material_ptr{ material_ptr_ }
	{}

	virtual bool hit(Ray const &r, double t_min, double t_max, HitRecord &rec) const override;

public:
	Vec3 centre;
	double radius;
	Material_p material_ptr;

};