#pragma once

#include <cmath>
#include "rtweekend.hpp"


class Vec3;

using Colour = Vec3;
//class Colour
//{
//public:
//	double r, g, b;
//};

class Vec3
{
public:
	Vec3(): e{0,0,0}{}
	Vec3(double e0, double e1, double e2):e{e0,e1,e2}{}

	double x() const { return e[0]; }
	double y() const { return e[1]; }
	double z() const { return e[2]; }

	// for colour
	double r() const { return e[0]; }
	double g() const { return e[1]; }
	double b() const { return e[2]; }


	Vec3 &operator+= (Vec3 const &other)
	{
		e[0] += other.e[0];
		e[1] += other.e[1];
		e[2] += other.e[2];

		return *this;
	}
	Vec3 &operator-= (Vec3 const &other)
	{
		e[0] -= other.e[0];
		e[1] -= other.e[1];
		e[2] -= other.e[2];

		return *this;
	}

	Vec3 &operator*= (double s)
	{
		e[0] *= s;
		e[1] *= s;
		e[2] *= s;
	
		return *this;
	}

	Vec3 &operator/= (double s)
	{
		return *this *= (1.0 / s);
	}

	Vec3 operator-() const
	{
		return Vec3(-x(), -y(), -z());
	}

	double length() const
	{
		return sqrt(lengthSquared());
	}

	double lengthSquared() const
	{
		return dot(*this);
	}

	double dot(Vec3 const &other) const
	{
		return
			e[0] * other.e[0] +
			e[1] * other.e[1] +
			e[2] * other.e[2];
	}

	static Vec3 random(double minv = 0, double maxv = 1)
	{
		return Vec3(
			randomRange(minv, maxv),
			randomRange(minv, maxv),
			randomRange(minv, maxv)
		);
	}

	Colour getColour(double scale) const
	{
		// Divide the colour total by the number of samples and gamma-correct
		//	for a gamma value of 2.0.
		auto r = sqrt(e[0] * scale);
		auto g = sqrt(e[1] * scale);
		auto b = sqrt(e[2] * scale);
		return Colour{
			256 * clamp(r, 0, 0.999),
			256 * clamp(g, 0, 0.999),
			256 * clamp(b, 0, 0.999)
		};
	}

	static Vec3 zero() { return Vec3{ 0, 0, 0 }; }

public:
	double e[3];
};


//inline std::ostream &operator<<(std::ostream &os, Vec3 const &v)
//{
//	return os << v.x() << ' ' << v.y() << ' ' << v.z();
//}

inline Vec3 operator+ (Vec3 const &v1, Vec3 const &v2)
{
	Vec3 v_result = v1;
	v_result += v2;
	return v_result;
}

inline Vec3 operator- (Vec3 const &v1, Vec3 const &v2)
{
	Vec3 v_result = v1;
	v_result -= v2;
	return v_result;
}

inline Vec3 operator* (Vec3 const &v1, double s)
{
	Vec3 v_result = v1;
	v_result *= s;
	return v_result;
}

inline Vec3 operator* (double s, Vec3 const &v1)
{
	return v1 * s;
}

/// member-wise multiplication
inline Vec3 operator* (Vec3 const &v1, Vec3 const &v2)
{
	return Vec3{ v1.x() * v2.x(), v1.y() * v2.y(), v1.z() * v2.z() };
}

inline Vec3 operator/ (Vec3 const &v1, double s)
{
	return v1 * (1 / s);
}

inline double dot(Vec3 const &v1, Vec3 const &v2)
{
	return v1.dot(v2);
}

inline Vec3 cross(Vec3 const &v1, Vec3 const &v2)
{
	return Vec3(
		v1.y() * v2.z() - v1.z() * v2.y(),
		v1.z() * v2.x() - v1.x() * v2.z(),
		v1.x() * v2.y() - v1.y() * v2.x()
	);
}

inline Vec3 unit(Vec3 const &v)
{
	return v / v.length();
}

inline Vec3 mix(Vec3 a, Vec3 b, double t)
{
	return (1 - t) * a + t * b;
}

inline Vec3 randomInUnitSphere()
{
	Vec3 p;
	do
	{
		p = Vec3::random(-1, 1);
	} while (p.lengthSquared() >= 1);

	return p;
}

inline Vec3 randomUnitVector()
{
	auto a = randomRange(0, 2 * pi);
	auto z = randomRange(-1, 1);
	auto r = sqrt(1 - z * z);
	return Vec3(r * cos(a), r * sin(a), z);
}

inline Vec3 randomInHemisphere(Vec3 const &normal)
{
	auto insphere = randomInUnitSphere();
	if (dot(insphere, normal) > 0)
		return insphere;
	else
		return -insphere;
}

inline Vec3 reflect(Vec3 n, Vec3 v)
{
	return v - 2 * dot(v, n) * n;
}

inline Vec3 refract(Vec3 n, Vec3 v, double coeff)
{
	Vec3 d = v + dot(v, n) * n;
	return v + d * (1 / coeff - 1);
}

inline Vec3 refract_book(Vec3 n, Vec3 v, double etai_over_etat)
{
	auto cos_theta = dot(-v, n);
	Vec3 r_out_parallel = etai_over_etat * (v + cos_theta * n);
	Vec3 r_out_perp = -sqrt(1 - r_out_parallel.lengthSquared()) * n;
	return r_out_parallel + r_out_perp;
}

inline Vec3 randomInUnitDisk()
{
	while (true)
	{
		auto p = Vec3(randomRange(-1, 1), randomRange(-1, 1), 0);
		if (p.lengthSquared() >= 1)
			continue;
		return p;
	}
}
