#include "rtweekend.hpp"
#include "Vec3.hpp"
#include "Ray.hpp"
#include "Hittable.hpp"
#include "HittableList.hpp"
#include "Sphere.hpp"
#include "Camera.hpp"
#include "ShinoStopwatch.hpp"
#include "Material.hpp"

#include <future>
#include <iostream>
#include <cassert>

using namespace std;

using point3 = Vec3;

Vec3 rayColour(Ray const &r, Hittable const &world, int depth)
{
	HitRecord rec;

	// If we've exceeded the ray bounce, no more light is gathered.
	if (depth <= 0)
	{
		return Vec3(0, 0, 0);
	}
	
	if (world.hit(r, 0.001, infinity, rec))
	{
		Ray scattered;
		Colour attenuation;
		if (rec.material_ptr->scatter(r, rec, attenuation, scattered))
		{
			return attenuation * rayColour(scattered, world, depth - 1);
		}
		else
		{
			return Colour{ 0,0,0 };
		}
	}

	auto unit_direction = unit(r.direction());
	auto t = 0.5 * (unit_direction.y() + 1.0);
	return mix({ 1,1,1 }, { 0.5,0.7,1.0 }, t);
}

class Renderer
{
	int samples_per_pixel;
	int width, height;
	std::vector<Vec3> fragments;
	Vec3 lookfrom, lookat;
	Camera cam;

public:
	Renderer(int width_, int height_, int samples_per_pixel_)
		: samples_per_pixel{ samples_per_pixel_ }
		, width{width_}, height{height_}
		, fragments(width * height, 
			Vec3::zero() // important to initialised to zero!
			//Vec3{1, 0,0}
			) 
		, lookfrom{ 13,2,3 }
		, lookat{ 0,0, 0 }
		, cam{ 
			lookfrom, lookat, {0, 1, 0}, 
			20, double(width_) / height_
			, .1 // aperture
			, 10
			}
	{}

	Vec3 *operator[](size_t y) { return &fragments[width * y]; }
	Vec3 const *operator[](size_t y) const { return &fragments[width * y]; }

	void renderRange(
		HittableList const &world,
		int yfirst, int ylast,
		int xfirst, int xlast,
		int max_depth)
	{
		for (auto y = ylast; y >= yfirst; --y)
		{
			for (auto x = xfirst; x <= xlast; ++x)
			{
				for (auto s = 0; s < samples_per_pixel; ++s)
				{
					auto u = (x + randomDouble()) / width;
					auto v = (y + randomDouble()) / height;

					Ray r = cam.getRay(u, v);
					auto sample_colour = rayColour(r, world, max_depth);
					(*this)[y][x] += (sample_colour
						////+ Vec3{ 0.1, 0, 0 }
						//+ Vec3{ 0, 0, 0.2 }
					);
				}
			}
		}
	}

	friend std::ostream &operator<< (std::ostream &os, Renderer const &fb)
	{
		double scale = 1.0 / fb.samples_per_pixel;

		os << "P3\n" << fb.width << " " << fb.height << "\n255'n";
		for (auto y = fb.height - 1; y >= 0; --y)
			for (auto x = 0; x < fb.width; ++x)
			{
				auto &pixel = fb[y][x].getColour(scale);

				os << static_cast<int>(pixel.r()) << ' '
					<< static_cast<int>(pixel.g()) << ' '
					<< static_cast<int>(pixel.b()) << '\n';
			}
		os << "\n";
		return os;
	}
};


void addRandomScene(HittableList &world)
{
	world.add(std::make_shared<Sphere>(Vec3{0, -1000, 0}, 1000,
	make_shared<Lambertian>(Colour{ .5, .5, .5 }) ));

	int i = 1;
	for (int a = -11; a < 11; ++a)
	{
		for (int b = -11; b < 11; ++b)
		{
			auto choose_mat = randomDouble();
			Vec3 centre{ a + 0.9 * randomDouble(), 0.2, b + 0.9 * randomDouble() };
			if ((centre - Vec3{ 4,.2,0 }).length() > 0.9)
			{
				if (choose_mat < 0.8)
				{
					// diffuse
					auto albedo = Colour::random() * Colour::random();
					world.add(make_shared<Sphere>(centre, 0.2,
						make_shared<Lambertian>(albedo)));
				}
				else if (choose_mat < 0.95)
				{
					// metal
					auto albedo = Colour::random(0.5, 1);
					auto fuzz = randomRange(0, .5);
					world.add(make_shared<Sphere>(centre, 0.2,
						make_shared<Metal>(albedo, fuzz)));
				}
				else
				{
					// glass
					world.add(make_shared<Sphere>(centre, 0.2,
						make_shared<Glass>(Colour::random(0.8, 1), 1.5, 0.3)));
				}
			}
		}
	}

	world.add(make_shared<Sphere>(Vec3{ 0,1,0 }, 1.0,
		make_shared<Glass>(Colour::random(0.8, 1), 1.5, 0.3)));

	world.add(make_shared<Sphere>(Vec3{ -4, 1, 0 }, 1,
		make_shared<Lambertian>(Colour{.4,.2,.1})));

	world.add(make_shared<Sphere>(Vec3{4,1,0}, 1,
		make_shared<Metal>(Colour{.7,.6,.5}, 0)));
}

int main()
{
	int const image_width = 320;
	int const image_height = 160;
	int const samples_per_pixel = 100;
	int const max_depth = 50;

	HittableList world;

	addRandomScene(world);
	//world.add(std::make_shared<Sphere>(Vec3{ 0, 0, -1 }, 0.5,
	//	make_shared<Lambertian>(Colour{ .1, .2, .5 }) ));
	//
	//// 'ground'
	//world.add(std::make_shared<Sphere>(Vec3{0, -100.5, - 1}, 100,
	//	make_shared<Lambertian>(Colour{ .8, .8, .0 }) ));

	//// NOTE adding a 'hollow' inner sphere 
	//world.add(std::make_shared<Sphere>(Vec3{ 1, 0, -1 }, 0.5,
	//	make_shared<Glass>(Colour{ .8, .6, .2 }, 1.5, 0.3)));
	//world.add(std::make_shared<Sphere>(Vec3{ 1, 0, -1 }, -0.49,
	//	make_shared<Glass>(Colour{ .8, .6, .2 }, 1.5, 0.3)));

	//world.add(std::make_shared<Sphere>(Vec3{ -1, 0, -1 }, 0.5,
	//	make_shared<Metal>(Colour{ .8, .6, .2 }, 0.3)));

	//auto R = cos(pi / 4);
	//world.add(make_shared<Sphere>(point3(-R, 0, -1), R, make_shared<Lambertian>(Colour(0, 0, 1))));
	//world.add(make_shared<Sphere>(point3(R, 0, -1), R, make_shared<Lambertian>(Colour(1, 0, 0))));
	
	Renderer renderer{ image_width, image_height, samples_per_pixel };

#define  PARALLEL_TASKS
	shino::precise_stopwatch stopwatch;

#ifdef PARALLEL_TASKS
	constexpr auto Y_BLOCK_SIZE = 32;
	constexpr auto X_BLOCK_SIZE = 32;
	vector<future<void>> futures; 
	// reserve using approx number of futures(!!)
	futures.reserve(image_height);

	// Queue tasks.
	for (auto y = image_height - 1; y >= 0; y -= Y_BLOCK_SIZE)
	{
		auto ylast = y, yfirst = std::max(y - (Y_BLOCK_SIZE-1), 0);

		for (auto x = 0; x < image_width; x += X_BLOCK_SIZE)
		{
			cerr << "+";
			auto xfirst = x, xlast = std::min(x + (X_BLOCK_SIZE - 1), image_width - 1);
			futures.emplace_back(
				async(launch::async,
					[&renderer, &world, 
					yfirst, ylast, 
					xfirst, xlast,
					image_width, max_depth]() { renderer.renderRange(world, yfirst, ylast, xfirst, xlast, max_depth); }
			));
		}
	}

	// Wait for tasks to complete.
	for (auto &f : futures)
	{
		f.get();
		cerr << "-";
	}
#else
	for (auto y = image_height - 1; y >= 0; --y)
	{
		cerr << "\rScanlines remaining: " << y << ' ' << std::flush;
		renderer.renderRange(world, y, y, 0, image_width - 1, max_depth);
	}
#endif // PARALLEL_TASKS

	cerr << "elapsed " << stopwatch.elapsed_time<int, chrono::milliseconds>() << " msec.\n";
	cerr << "\n";
	cerr << "Writing output.\n";
	cout << renderer;
	cerr << "Done.\n";
}
