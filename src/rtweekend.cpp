#include "rtweekend.hpp"

#include <random>

double randomDouble()
{
	static auto rand_generator =
		[	gen = std::mt19937{},
			distribution = std::uniform_real_distribution<double>{ 0.0, 1.0 }
		] 
		() mutable
		{ 
			return distribution(gen);
		};

	return rand_generator();
}
