#pragma once

#include <limits>


constexpr auto infinity = std::numeric_limits<double>::infinity();

//#define _USE_MATH_DEFINES
//#include <cmath>
constexpr auto pi = 3.1415926535897932385;


inline constexpr double degToRad(double degrees)
{
	return degrees * pi / 180;
}

inline constexpr double ffmin(double a, double b) { return a <= b ? a : b; }
inline constexpr double ffmax(double a, double b) { return a >= b ? a : b; }

inline constexpr double clamp(double x, double minv, double maxv)
{
	return ffmin(maxv, ffmax(minv, x));
}


double randomDouble();

inline double randomRange(double minv, double maxv)
{
	return randomDouble() * (maxv - minv) + minv;
}
